<?php

// подключаем класс
require __DIR__ . '/lib/classes/Uploader.php';

// создаем объект
$userUpload = new Uploader('userFile');
$savePath = __DIR__ . '/lib/uploads';

// выводим результат
var_dump($userUpload->setSavePath($savePath)->upload());