<?php

// подключаем классы
require __DIR__ . '/lib/classes/TextFile.php';
require __DIR__ . '/lib/classes/GuestBook.php';

// создаем объект
$gbPath = __DIR__ . '/lib/data.txt';
$guestBook = new GuestBook($gbPath);
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>

<h1>GuestBook:</h1>

<?php foreach ($guestBook->getAllRecords() as $record) { ?>
    <p><?php echo $record->getText(); ?></p>
<?php } ?>

<form action="/save.php" method="post">
    <input type="text" name="message">
    <button submit="submit">Send</button>
</form>

<h1>File upload</h1>
<form action="/saveFile.php" method="post" enctype="multipart/form-data">
    <input type="file" name="userFile" >
    <button submit="submit">Save file</button>
</form>

</body>
</html>
