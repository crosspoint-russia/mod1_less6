<?php

/**
Создайте класс Uploader в соответствии с требованиями:
В конструктор передается имя поля формы, от которого мы ожидаем загрузку файла
Метод isUploaded() проверяет - был ли загружен файл от данного имени поля
Метод upload() осуществляет перенос файла (если он был загружен!) из временного места в постоянное
 */

class Uploader
{
    // имя поля формы
    protected $fieldName;
    // информация по загружаемому файлу
    protected $fileData;
    // путь сохранения
    protected $savePath;

    // конструктор
    public function __construct($fieldName)
    {
        $this->fieldName = $fieldName;
    }

    // Метод isUploaded() проверяет - был ли загружен файл от данного имени поля
    // Используется только внутри класса, закрываем в protected
    protected function isUploaded()
    {
        if (isset($_FILES[$this->fieldName])) {
            if (0 == $_FILES[$this->fieldName]['error']) {
                $this->fileData = $_FILES[$this->fieldName];
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    // Метод upload() осуществляет перенос файла (если он был загружен!) из временного места в постоянное
    public function upload()
    {
        if ($this->isUploaded()) {

            if (move_uploaded_file($this->fileData['tmp_name'], $this->savePath . '/' . $this->fileData['name'])) {
                return $this->fileData['name'];
            } else {
                return null;
            }

        } else {
           return null;
        }

    }

    // метод для установки папки сохранения
    public function setSavePath($path) {
        $this->savePath = $path;
        return $this;
    }

}