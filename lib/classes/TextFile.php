<?php

/**
Продумайте - какие части функционала можно вынести в базовый (родительский) класс TextFile, 
а какие - сделать в унаследованном от него классе GuestBook
 */

class TextFile
{
    // защищенное свойство объекта для хранения данных
    protected $path;
    protected $records = [];

    // В конструктор передается путь до файла, в нём же происходит чтение данных
    // (используйте защищенное свойство объекта для хранения данных)
    public function __construct($path)
    {
        $this->path = $path;
    }

    // Метод getAllRecords() возвращает массив записей из файла
    // публичный т.к. пользуемся методом вне класса
    public function getAllRecords()
    {
        return $this->records;
    }

    // Метод save() сохраняет массив в файл
    // публичный т.к. пользуемся методом вне класса
    public function save()
    {
        $dataString = implode("\n", $this->records);
        file_put_contents($this->path, $dataString);
    }

}