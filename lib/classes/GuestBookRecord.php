<?php

/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 18.12.2016
 * Time: 18:19
 */
class GuestBookRecord
{
    protected $text;

    function __construct($text)
    {
        $this->text = $text;
    }

    public function getText()
    {
        return $this->text;
    }

}