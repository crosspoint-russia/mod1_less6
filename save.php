<?php

// подключаем классы
require __DIR__ . '/lib/classes/TextFile.php';
require __DIR__ . '/lib/classes/GuestBook.php';

// создаем объект
$gbPath = __DIR__ . '/lib/data.txt';
$guestBook = new GuestBook($gbPath);

if (isset($_POST['message']) && '' != $_POST['message']) {

    // метод append возвращает нам $this т.е. ссылку на текущий экземпляр.
    // у объектов есть свойства и методы, соответственно, мы можем вместо отдельного вызова методов
    // $guestBook->append($_POST['message']);
    // $guestBook->save();
    // дернуть их цепочкой!
    // и так до тех пор пока результатом метода возвращается ссылка на текущий экземпляр
    $gbRecord = new GuestBookRecord($_POST['message']);
    $guestBook
        ->append($gbRecord)
        ->save();
        
}

header('Location: /');
die();